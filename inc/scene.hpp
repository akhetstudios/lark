/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#include <coord.hpp>
#include <SDL2/SDL.h>

#include <set>
#include <vector>

struct RGBa {
  unsigned int R, G, B, a;
};

struct Obj : public Coord {
  std::vector<RGBa> Pixels;
  virtual void Bump(SDL_Event& event) = 0;
  
  Obj(Coord C);
  virtual ~Obj() = default;
};

struct Scene : public Coord {
  std::set<Obj*> Objects;
  SDL_Texture* T;
	Scene(
    Coord C,
		SDL_Renderer* R
	);
  void Update(
      SDL_Rect area, 
      std::vector<unsigned int> p
      );
  void Update(
      int x, int y,
      unsigned int p
      ); 
 	~Scene();
};
