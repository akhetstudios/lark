/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <SDL2/SDL.h>
#include <scene.hpp>
#include <render.hpp>

#include <atomic>
#include <string>
#include <map>
#include <array>
#include <deque>

using uint2 = std::array<unsigned int, 2>;

struct Window {
private:
  SDL_Window* window;
	std::atomic<bool> quit{false};
public:
  SDL_Renderer* renderer;
  
  bool Quit();
  void Quit(bool state);

  uint2 Dimensions(); // w, h
  void Clear(); // clear renderer
  void Draw(); // apply renderer 

  Window (
		std::string title,
		unsigned int x,
		unsigned int y
	);

  void Close(); // only call this after EVERYTHING has been cleaned up  

  struct EventManager {
  private:
    Window* W;
    std::map<int, std::set<Obj*>> Notify;
  public:   
    void Install(int event, Obj* object);
    void Remove(int event, Obj* object); // YOU are responsible for cleaning up Objects in the EventManager on Scene destruction
    
    void Listen();
    
    EventManager(Window* W);
  } Events{this};
};

