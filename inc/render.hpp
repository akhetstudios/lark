/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/


#pragma once

#include<scene.hpp>
#include<coord.hpp>
#include<SDL2/SDL.h>

#include<vector>

struct RScene : public Coord { // wrapper for scene render
  Scene*  Origin;   
  RScene(Scene* S, Coord C);
};

// RStack: unified render stack
// you can exclusively append/pop the stack (it's a stack...):
//  if you have to pop more than half of the stack (this is arbitrary, but demonstrates the general principle), consider re-forming it
// Rstack maintains a global coordinate set; 
//  this is used to determine offsets during rendering
//
// Use RStack::Visible to get/set the visible section of the world (relative to the world, not the screen)

struct RStack {
private: 
  void update(
      int x_delta = 0,
      int y_delta = 0,
      int w_delta = 0,
      int h_delta = 0
      ); // used to update the layers when global pos has changed
public:
  Coord World{0, 0, 0, 0}; // Global Plane 
  std::vector<RScene> Layers;

  void Add(RScene R); // R's coord should be set relative to the world plane by the game itself
  void Pop();
  
  void Draw(SDL_Renderer* R); // more like compose...

  Coord Visible{0, 0, 0, 0}; 

  RStack(std::vector<RScene> RV);
  RStack();
};
