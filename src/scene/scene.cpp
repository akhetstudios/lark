/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/

#include <scene.hpp>
#include <debug.hpp>

#include <cstring>

Scene::Scene(
    Coord C,
    SDL_Renderer* R
  ) : Coord(C) {
  this->T = SDL_CreateTexture(
      R,
      SDL_PIXELFORMAT_ARGB8888,
      SDL_TEXTUREACCESS_STREAMING,
      Coord::w,
      Coord::h
    ); 
}
void
Scene::Update(
  SDL_Rect area,
  std::vector<unsigned int> p
) {
  void* pixels;
  int w = Coord::w;
  debug.checkfail(SDL_LockTexture(this->T, &area, &pixels, &w));
  if (p.size() != (unsigned int) area.w * area.h) debug.fail("Scene::Update| New pixel array isn't correctly sized");
  std::memcpy(pixels, p.data(), p.size());
  SDL_UnlockTexture(this->T);
}

void
Scene::Update(
  int x, int y,
  unsigned int p
) {
  this->Update(SDL_Rect{x, y, 1, 1}, {p});
}

Scene::~Scene() {
  SDL_DestroyTexture(T);
}
