/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/

#include <window.hpp>
#include <debug.hpp>

void 
Window::EventManager::Install(
    int event,
    Obj* object
  ) {
  Notify[event].insert(object);
}

void 
Window::EventManager::Remove(
    int event,
    Obj* object
  ) {
  if (Notify[event].count(object)) {
    Notify[event].erase(object);
  } else {
    debug.update("Attempt to erase non-existant object"); // TODO: might require more information...
  } 
}

void
Window::EventManager::Listen() {
  SDL_Event event;
  while( !(W->Quit()) ) {
    SDL_WaitEvent(&event);
    switch(event.type) {
      case SDL_QUIT:
        W->Quit(true);
        break;
      default:
        for (const auto& o : Notify[event.type]) {
          o->Bump(event);
        }
        break;
    }
  }
};

Window::EventManager::
EventManager(Window* W) : W(W) {}
