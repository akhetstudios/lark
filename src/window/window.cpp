/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <window.hpp>
#include <config.hpp>
#include <debug.hpp>

Window::Window(
	std::string title,
	unsigned int x,
	unsigned int y
) {
	debug.checkfail(SDL_CreateWindowAndRenderer(x, y, WINDOW_FLAGS, &window, &renderer));	
}

void
Window::Close() {
  SDL_DestroyWindow(window);
  SDL_Quit();
}

bool
Window::Quit() {
  return this->quit.load();
}

void
Window::Quit(bool s) {
  this->quit.store(s);
}

uint2
Window::Dimensions() {
  int w, h;
  SDL_GetWindowSize(window, &w, &h);
  return {static_cast<unsigned int>(w), static_cast<unsigned int>(h)};
}

void
Window::Clear() {
  SDL_RenderClear(renderer);
}

void
Window::Draw() { 
  SDL_RenderPresent(renderer);
}

