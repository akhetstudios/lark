/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/

#include<render.hpp>

RScene::
RScene(Scene* S, Coord C) 
  : Coord(C), Origin(S) {
}

void
RStack::update(
  int x_d, int y_d,
  int w_d, int h_d
  ) {
  // update layer offsets
  for (auto& L : Layers) {
    L.x += x_d;
    L.y += y_d;
    L.w += w_d;
    L.h += h_d;
  }
  // update world
  World.x += x_d;
  World.y += y_d;
  World.w += w_d;
  World.h += h_d;
}

void
RStack::Add(RScene R) {
  // we need to update the offset(s) if this layer will be the max
  if (World.w < R.w) {
    update(0, 0, (R.w - World.w), 0);
  }
  if (World.h < R.h) {
    update(0, 0, 0, (R.w - World.w));
  }
}

void
RStack::Pop() { // we don't re-calculate the offset; if you have a seriouslly obscene difference between any two given layers, consider seperating them into different stacks 
  Layers.pop_back();
}

void
RStack::Draw(SDL_Renderer* R) {
  SDL_Texture* Composite 
    = debug.checkfail(
    SDL_CreateTexture(
      R,
      SDL_PIXELFORMAT_ARGB8888,
      SDL_TEXTUREACCESS_STREAMING,
      Visible.w,
      Visible.h
    )
  );
  for (unsigned int i = 1; i<Layers.size(); i++) {
    Scene* a = Layers.at(i).Origin;
    Scene* b = Layers.at(i-1).Origin;
    // apply compsiting <https://en.wikipedia.org/wiki/Alpha_compositing>
    
  } 
}

RStack::
RStack(std::vector<RScene> RV) : Layers(RV) {
  for (auto& L : RV) {Add(L);}
};

RStack::
RStack() {};
