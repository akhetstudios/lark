/**
 * This file is part of The Lark Engine.
 *
 * The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.
*/

#include <SDL2/SDL.h>
#include <debug.hpp>

void
__debug::update(std::string msg) {
	if (enabled) log.push_back(msg);
}

void 
__debug::fail(std::string msg) {
	this->update(msg);
  W->Quit(true);
}

void
__debug::checkfail(int value) {
  if (value == 0) return; 
  this->fail(SDL_GetError() ? std::string(SDL_GetError()) : "Unknown or Non-SDL Error"); 
  SDL_ClearError();
}

void
__debug::checkfail(void* ptr) {
  if (!ptr) checkfail(-1);
}
