The Lark Engine
===

Please submit patches to the mailing list: "lark+patches[at]akhetstudios.com".
(Non-FAQ) Questions can be directed to: "lark+qna[at]akhetstudios.com".
Development Secific Questions should be sent to: "lark+dev[at]akhetstudios.com".

FAQ
===

Licensing: 
  The Lark Engine (TLE) is licensed under GNU GPL v3 (or later): <LICENSE.txt>.
  Simple DirectMedia Layer (SDL), which The Lark Engine requires as a dependency, is licensed under zlib: <https://www.libsdl.org/license.php>.

"Why do you do it (this) way?":
  TSOE was intentionally developed 'blind'; no referential material strictly relevant to game-engine design was used. It is hoped that this produces the simplist, most direct engine for Sierra Online. If you think we're missing something, please make sure to let us know on the mailing list[1].



Foot/End Notes
===
[1]: See; Development Specific Questions
