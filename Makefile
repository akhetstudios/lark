
# This file is part of The Lark Engine.
#
# The Lark Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Lark Engine is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with The Lark Engine. If not, see <https://www.gnu.org/licenses/>.

default: linux

LINK = -lSDL2
G = g++
W = -Wall
F = -std=c++17 $(W) -MMD -MP -c

B = build/bin/

## Source/Headers/Object
HDR = $(wildcard inc/*.hpp)
SRC = $(wildcard src/*/*.cpp src/*.cpp)
OBJ = $(wildcard build/bin/*.o)

## build objects
## - generate fresh dependency
## - build objects from dependency file

OBUILD:
	@echo "== Building =="
	@$(foreach f,$(SRC),\
		$(G) -c $(F) \
		$f -o $(B)$(lastword $(subst /, , $(basename $f))).o -I./inc; \
		echo "$f | Built"; \
		)


linux: OBUILD $(HDR)
	@echo "== Linking Program =="
	@ar rvs build/libtsoe.a $(OBJ)
	@echo "== Complete =="

clean:
	rm -f build/*.a
	rm -f build/bin/*.o
